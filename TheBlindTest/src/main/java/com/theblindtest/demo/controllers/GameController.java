package com.theblindtest.demo.controllers;
import com.theblindtest.demo.entities.Game;
import com.theblindtest.demo.services.GameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin
@RestController
public class GameController {

    Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameService gameService;

    @Operation(summary = "Ajoute une Game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game Ajouter !", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Game.class)) }),
            @ApiResponse(responseCode = "400", description = "La requête pour ajouter la Game n'a pas pu aboutir", content = @Content),
            @ApiResponse(responseCode = "500", description = "Erreur Interne, Serveur Inaccessible", content = @Content) })
    @PostMapping("api/v1/addGame")
    public Game addGame(@RequestBody Game game){

        logger.info( "Appel de la méthode addGame");

        return gameService.saveGame(game);
    }

    @Operation(summary = "Récupére toute les parties")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Les parties ont été récupéré !", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Game.class)) }),
            @ApiResponse(responseCode = "400", description = "La requête pour récupérer les Parties n'a pas pu aboutir", content = @Content),
            @ApiResponse(responseCode = "404", description = "Les Parties démandé sont Introuvables", content = @Content),
            @ApiResponse(responseCode = "500", description = "Erreur Interne, Serveur Inaccessible", content = @Content) })
    @GetMapping("api/v1/games")
    public List<Game> findAllGames(){

        logger.info( "Appel de la méthode findAllGames");

        return gameService.getGames();
    }

    @Operation(summary = "Récupére la game avec son identifiant en paramètre")
    @Parameter(name = "id", description = "Identifiant de la game", example = "1")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La Game a été récupéré !", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Game.class)) }),
            @ApiResponse(responseCode = "400", description = "La requête pour récupérer la Game avec son Id n'a pas pu aboutir", content = @Content),
            @ApiResponse(responseCode = "404", description = "La Game démandé est Introuvable", content = @Content),
            @ApiResponse(responseCode = "500", description = "Erreur Interne, Serveur Inaccessible", content = @Content) })
    @GetMapping("api/v1/game/{id}")
    public Game findGameById(@PathVariable String id){

        logger.info( "Appel de la méthode findGameById avec l'id " + id);

        return gameService.getGameById(id);
    }

    @Operation(summary = "Met à jour la game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La Game a été mis à jour !", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Game.class)) }),
            @ApiResponse(responseCode = "400", description = "La requête pour mettre à jour la Game n'a pas pu aboutir", content = @Content),
            @ApiResponse(responseCode = "404", description = "La Game démandé est Introuvable", content = @Content),
            @ApiResponse(responseCode = "500", description = "Erreur Interne, Serveur Inaccessible", content = @Content) })
    @PutMapping("api/v1/updateGame")
    public Game updateGame(@RequestBody Game game){

        logger.info( "Appel de la méthode updateGame avec la game " + game);

        return gameService.updateGame(game);
    }

    @Operation(summary = "Supprime la game")
    @Parameter(name = "id", description = "Identifiant de la game", example = "1")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La Game a bien été supprimé !", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Game.class)) }),
            @ApiResponse(responseCode = "400", description = "La requête pour supprimé la Game n'a pas pu aboutir", content = @Content),
            @ApiResponse(responseCode = "404", description = "La Game démandé est Introuvable", content = @Content),
            @ApiResponse(responseCode = "500", description = "Erreur Interne, Serveur Inaccessible", content = @Content) })
    @DeleteMapping("api/v1/deleteGame/{id}")
    public String deleteGame(@PathVariable String id){

        logger.info( "Appel de la méthode deleteGame avec l'id " + id);

        return gameService.deleteGame(id);
    }

}
