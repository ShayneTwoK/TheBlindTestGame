package com.theblindtest.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.theblindtest.demo.services.SpotifyService;
import reactor.core.publisher.Mono;

@RestController
public class SpotifyController {

    private final SpotifyService spotifyService;

    public SpotifyController(SpotifyService spotifyService) {
        this.spotifyService = spotifyService;
    }

    @GetMapping("/tracks/{id}")
    public Mono<String> getTrack(@PathVariable String id) {
        return spotifyService.getAccessToken()
                .flatMap(accessToken -> {
                    WebClient spotifyClient = WebClient.builder()
                            .baseUrl("https://api.spotify.com")
                            .defaultHeader("Authorization", "Bearer " + accessToken)
                            .build();

                    return spotifyClient.get()
                            .uri("/v1/tracks/" + id)
                            .retrieve()
                            .bodyToMono(String.class);
                });
    }

    @GetMapping("/playlists/{id}/tracks")
    public Mono<String> getPlaylist(@PathVariable String id) {
        return spotifyService.getAccessToken()
                .flatMap(accessToken -> {
                    WebClient spotifyClient = WebClient.builder()
                            .baseUrl("https://api.spotify.com")
                            .defaultHeader("Authorization", "Bearer " + accessToken)
                            .build();

                    return spotifyClient.get()
                            .uri("/v1/playlists/" + id + "/tracks")
                            .retrieve()
                            .bodyToMono(String.class);
                });
    }
}
