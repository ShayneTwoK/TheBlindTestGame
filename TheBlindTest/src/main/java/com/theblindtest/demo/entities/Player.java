package com.theblindtest.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import jakarta.persistence.CascadeType;

import org.hibernate.annotations.UuidGenerator;

import jakarta.persistence.*;

// On import jakarta pour les bdd "classique" comme H2, MySQL, Oracle.. avec JPA et springframework.data pour d'autre bdd comme MongoDB

// @Document se definit comme l'annotation JPA @Entity
// @Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Player {

    @Id
    @UuidGenerator
    private String id;
    private String name;
    private String avatar;
    private Integer point;
    private Boolean isHost;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room currentRoom;

    public Player(String name, String avatar, Integer point, Boolean isHost, Room currentRoom) {
        this.name = name;
        this.avatar = avatar;
        this.point = point;
        this.isHost = isHost;
        this.currentRoom = currentRoom;
    }
}
