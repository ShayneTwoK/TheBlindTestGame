package com.theblindtest.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import jakarta.persistence.*;
import java.util.List;

import org.hibernate.annotations.UuidGenerator;

// On import jakarta pour les bdd "classique" comme H2, MySQL, Oracle.. avec JPA et springframework.data pour d'autre bdd comme MongoDB

// @Document se definit comme l'annotation JPA @Entity
// @Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Room {

    @Id
    @UuidGenerator
    private String id;
    private String code;
    private String date_creation;
    private String game_mode; // BUZZER, FIRST!, MASTER, DRAWSICAL
    private String game_style; // DYSNEY, FILMS, JV, FR_VARIETY, CARTOON, 80S, 90S, 2000S, CUSTOM
    private String link_invitation;
    private String room_configuration;

    // List<> ordonné et indexé, Map<> entity/key, Collection<>, Set<> et SortedSet<> 
    @OneToMany(mappedBy = "currentRoom", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Player> playersRoom;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Game> games;

    public Room(String code, String date_creation, String game_mode, String game_style, String link_invitation, String room_parameters) {
        this.code = code;
        this.date_creation = date_creation;
        this.game_mode = game_mode;
        this.game_style = game_style;
        this.link_invitation = link_invitation;
        this.room_configuration  = room_parameters;
    }

    public void addPlayerInRoom(Player player) {
        playersRoom.add(player);
        player.setCurrentRoom(this);  // On met à jour la relation bidirectionnelle
    }

}
