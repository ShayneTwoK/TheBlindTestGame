package com.theblindtest.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.UuidGenerator;

import jakarta.persistence.*;

// On import jakarta pour les bdd "classique" comme H2, MySQL, Oracle.. avec JPA et springframework.data pour d'autre bdd comme MongoDB

// @Document se definit comme l'annotation JPA @Entity
// @Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Game {

    @Id
    @UuidGenerator
    private String id;
    private String date_creation;
    private String status; // NOT_STARTED, STARTED, FINISHED
    private String winner;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    public Game(String date_creation, String status, String winner, Room room) {
        this.date_creation = date_creation;
        this.status = status;
        this.winner = winner;
        this.room = room;
    }
}
