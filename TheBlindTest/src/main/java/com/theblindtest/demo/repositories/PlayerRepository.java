package com.theblindtest.demo.repositories;

import com.theblindtest.demo.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {

    Player findByName(String name);

}
