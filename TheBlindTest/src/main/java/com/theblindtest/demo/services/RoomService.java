package com.theblindtest.demo.services;
import com.theblindtest.demo.entities.Room;
import com.theblindtest.demo.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoomService {
    @Autowired
    private RoomRepository roomRepository;

    public Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

    public List<Room> getRooms() {
        return roomRepository.findAll();
    }

    public Room getRoomById(String id) {
        return roomRepository.findById(id).orElse(null);
    }

    public Room updateRoom(String id, Room room) {

        boolean isRoomExist = roomRepository.existsById(id);

        if (!isRoomExist) {
            throw new IllegalStateException("la room avec l'id " + id + " n'existe pas.");
        }

        Room existingRoom = roomRepository.findById(id).orElse(null);

        existingRoom.setCode(room.getCode());
        existingRoom.setPlayersRoom(room.getPlayersRoom());
        
        return roomRepository.save(existingRoom);
    }

    public String deleteRoom(String id) {

        boolean isRoomExist = roomRepository.existsById(id);

        if (!isRoomExist) {
            throw new IllegalStateException("la room avec l'id " + id + " n'existe pas.");
        }

        roomRepository.deleteById(id);

        return "room " + id + " supprimé !";
    }

}
