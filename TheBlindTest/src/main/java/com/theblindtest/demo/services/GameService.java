package com.theblindtest.demo.services;
import com.theblindtest.demo.entities.Game;
import com.theblindtest.demo.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;

    public Game saveGame(Game game) {
        return gameRepository.save(game);
    }

    public List<Game> getGames() {
        return gameRepository.findAll();
    }

    public Game getGameById(String id) {
        return gameRepository.findById(id).orElse(null);
    }

    public Game updateGame(Game game) {

        boolean isGameExist = gameRepository.existsById(game.getId());

        if (!isGameExist) {
            throw new IllegalStateException("la game avec l'id " + game.getId() + " n'existe pas.");
        }

        Game existingGame = gameRepository.findById(game.getId()).orElse(null);

        existingGame.setRoom(game.getRoom());
        existingGame.setWinner(game.getWinner());

        return gameRepository.save(existingGame);
    }

    public String deleteGame(String id) {

        boolean isGameExist = gameRepository.existsById(id);

        if (!isGameExist) {
            throw new IllegalStateException("la game avec l'id " + id + " n'existe pas.");
        }

        gameRepository.deleteById(id);

        return "game " + id + " supprimé !";
    }
}
