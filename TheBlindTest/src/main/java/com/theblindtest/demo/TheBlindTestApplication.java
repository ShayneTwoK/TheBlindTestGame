package com.theblindtest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheBlindTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheBlindTestApplication.class, args);
	}

}
