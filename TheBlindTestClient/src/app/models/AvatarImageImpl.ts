export interface AvatarImage {
    id: number;
    url: string;
    pseudoRandom: string;
}
