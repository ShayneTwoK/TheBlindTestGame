import { RoomDTO } from "../room/RoomDTO";

export class PlayerDTO {
    id?: string;        // Optional because it might not be set in forms
    name: string;
    avatar: string;
    point?: number;
    isHost?: boolean;
    currentRoom?: RoomDTO;
  
    constructor(
      name: string,
      avatar: string,
      point?: number,
      isHost?: boolean,
      currentRoom?: RoomDTO,
      id?: string
    ) {
      this.name = name;
      this.avatar = avatar;
      this.point = point;
      this.isHost = isHost;
      this.currentRoom = currentRoom;
      this.id = id;
    }
  
    // Method to convert DTO to backend payload format
    toBackendPayload(): any {
      return {
        id: this.id,
        name: this.name,
        avatar: this.avatar,
        point: this.point,
        isHost: this.isHost,
        currentRoom: this.currentRoom
      };
    }
  }
  