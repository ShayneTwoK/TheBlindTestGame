import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs";
import { PlayerDTO } from './PlayerDTO';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private apiServerUrl: string = "http://localhost:8080/api/v1";

  constructor(private http: HttpClient) {

  }

  public getPlayers(): Observable<PlayerDTO[]> {
    return this.http.get<PlayerDTO[]>(`${this.apiServerUrl}/players`);
  }

  public getPlayerById(id: string): Observable<PlayerDTO> {
    return this.http.get<PlayerDTO>(`${this.apiServerUrl}/player/${id}`);
  }

  public addPlayer(player: PlayerDTO): Observable<PlayerDTO> {
    return this.http.post<PlayerDTO>(`${this.apiServerUrl}/addPlayer`, player);
  }

  public updatePlayer(id: string, player: PlayerDTO): Observable<PlayerDTO> {
    return this.http.put<PlayerDTO>(`${this.apiServerUrl}/updatePlayer/${id}`, player);
  }

  public deletePlayer(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/deletePlayer/${id}`);
  }

}
