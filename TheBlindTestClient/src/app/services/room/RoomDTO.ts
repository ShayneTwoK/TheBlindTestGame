import { GameDTO } from "../game/GameDTO";
import { PlayerDTO } from "../player/PlayerDTO";

export class RoomDTO {
    id?: string;
    code: string;
    date_creation: string;
    game_mode?: string;
    game_style?: string;
    link_invitation?: string;
    room_configuration?: string;
    playersRoom?: PlayerDTO[];
    games?: GameDTO[];
  
    constructor(
      code: string,
      date_creation: string,
      game_mode?: string,
      game_style?: string,
      link_invitation?: string,
      room_configuration?: string,
      playersRoom?: PlayerDTO[],
      games?: GameDTO[],
      id?: string
    ) {
      this.code = code;
      this.date_creation = date_creation;
      this.game_mode = game_mode;
      this.game_style = game_style;
      this.link_invitation = link_invitation;
      this.room_configuration = room_configuration;
      this.playersRoom = playersRoom;
      this.games = games;
      this.id = id;
    }
  
    // Method to convert DTO to backend payload format
    toBackendPayload(): any {
      return {
        id: this.id,
        code: this.code,
        date_creation: this.date_creation,
        game_mode: this.game_mode,
        game_style: this.game_style,
        link_invitation: this.link_invitation,
        room_configuration: this.room_configuration,
        playersRoom: this.playersRoom,
        games: this.games,
      };
    }
  }
  