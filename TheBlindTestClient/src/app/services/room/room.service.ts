import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RoomDTO } from './RoomDTO';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  private apiServerUrl: string = "http://localhost:8080/api/v1";

  constructor(private http: HttpClient) {

  }

  // public getPlayers(): Observable<PlayerDTO[]> {
  //   return this.http.get<PlayerDTO[]>(`${this.apiServerUrl}/players`);
  // }

  public addRoom(room: RoomDTO): Observable<RoomDTO> {
    return this.http.post<RoomDTO>(`${this.apiServerUrl}/addRoom`, room);
  }

  public updateRoom(id: string, room: RoomDTO): Observable<RoomDTO> {
    return this.http.put<RoomDTO>(`${this.apiServerUrl}/updateRoom/${id}`, room);
  }

  // public deletePlayer(playerId: number): Observable<void> {
  //   return this.http.delete<void>(`${this.apiServerUrl}/deletePlayer/${playerId}`);
  // }
  
}
