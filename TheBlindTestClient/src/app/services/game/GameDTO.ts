import { RoomDTO } from "../room/RoomDTO";

export class GameDTO {
    id?: string;
    date_creation: string;
    room: RoomDTO;
    status?: string;
    winner?: string;
  
    constructor(
      date_creation: string,
      room: RoomDTO,
      status?: string,
      winner?: string,
      id?: string
    ) {
      this.date_creation = date_creation;
      this.room = room;
      this.status = status;
      this.winner = winner;
      this.id = id;
    }
  
    // Method to convert DTO to backend payload format
    toBackendPayload(): any {
      return {
        id: this.id,
        date_creation: this.date_creation,
        room: this.room,
        status: this.status,
        winner: this.winner,
      };
    }
  }
  