import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
// Service qui permet de stocker les donnees utilisateur sur son peripherique local
export class LocalStorageService {

  constructor() { }

  // Sauvegarder une donnee dans le localStorage
  setItem(key: string, value: any): void {
    localStorage.setItem(key, value);
  }

  // Recuperer une donnee depuis le localStorage
  getItem(key: string): string | null {
    return localStorage.getItem(key);
  }

  // Supprimer une donnee depuis le localStorage
  removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  // Supprimer toutes les donnees du localStorage
  clear(): void {
    localStorage.clear();
  }

}
