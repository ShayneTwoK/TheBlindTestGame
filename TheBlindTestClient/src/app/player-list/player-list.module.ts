import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerListRoutingModule } from './player-list-routing.module';
import { PlayerListComponent } from './player-list.component';
import { PlayerService } from '../services/player/player.service';

@NgModule({
  declarations: [PlayerListComponent],
  exports: [PlayerListComponent],
  imports: [CommonModule, PlayerListRoutingModule],
  providers: [PlayerService],
})
export class PlayerListModule {}
