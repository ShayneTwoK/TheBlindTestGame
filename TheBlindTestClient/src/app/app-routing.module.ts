import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  // Met le composant "home" à la racine du site sans afficher l'url "/home"
  { path: '', component: HomeComponent }, // Eager Loading
  // { path: "", redirectTo: '/home', pathMatch: 'full'},
  // { path: "home", component: HomeComponent },
  { path: "room", loadChildren: () => import('./room/room.module').then(m => m.RoomModule) }, // Lazy Loading
  { path: "game", loadChildren: () => import('./game/game.module').then(m => m.GameModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
