import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { LocalStorageService } from '../services/local-storage/local-storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AvatarImage } from '../models/AvatarImageImpl';
import { Subscription } from 'rxjs';
import { PlayerDTO } from '../services/player/PlayerDTO';
import { PlayerService } from '../services/player/player.service';
import { RoomService } from '../services/room/room.service';
import { RoomDTO } from '../services/room/RoomDTO';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {

  avatarsImage: AvatarImage[] = [
    { id: 1, url: 'https://garticphone.com/images/avatar/0.svg', pseudoRandom: 'WrestlerCool' + this.setRandomInt(0,9999)},
    { id: 2, url: 'https://garticphone.com/images/avatar/1.svg', pseudoRandom: 'AlienCool' + this.setRandomInt(0,9999) },
    { id: 3, url: 'https://garticphone.com/images/avatar/2.svg', pseudoRandom: 'StrawberryCool' + this.setRandomInt(0,9999) },
    { id: 4, url: 'https://garticphone.com/images/avatar/3.svg', pseudoRandom: 'DonutCool' + this.setRandomInt(0,9999) },
    { id: 5, url: 'https://garticphone.com/images/avatar/4.svg', pseudoRandom: 'MummyCool' + this.setRandomInt(0,9999) },
    { id: 6, url: 'https://garticphone.com/images/avatar/5.svg', pseudoRandom: 'PizzaCool' + this.setRandomInt(0,9999) },
    { id: 7, url: 'https://garticphone.com/images/avatar/6.svg', pseudoRandom: 'GeloCool' + this.setRandomInt(0,9999) },
    { id: 8, url: 'https://garticphone.com/images/avatar/7.svg', pseudoRandom: 'OctopusCool' + this.setRandomInt(0,9999) },
    { id: 9, url: 'https://garticphone.com/images/avatar/8.svg', pseudoRandom: 'PumpkinCool' + this.setRandomInt(0,9999) },
    { id: 10, url: 'https://garticphone.com/images/avatar/9.svg', pseudoRandom: 'BrushCool' + this.setRandomInt(0,9999) },
    { id: 11, url: 'https://garticphone.com/images/avatar/10.svg', pseudoRandom: 'BurgerCool' + this.setRandomInt(0,9999) },
    { id: 12, url: 'https://garticphone.com/images/avatar/11.svg', pseudoRandom: 'SundaeCool' + this.setRandomInt(0,9999) },
    { id: 13, url: 'https://garticphone.com/images/avatar/12.svg', pseudoRandom: 'ZombieCool' + this.setRandomInt(0,9999) },
    { id: 14, url: 'https://garticphone.com/images/avatar/13.svg', pseudoRandom: 'PineappleCool' + this.setRandomInt(0,9999) },
    { id: 15, url: 'https://garticphone.com/images/avatar/14.svg', pseudoRandom: 'FishCool' + this.setRandomInt(0,9999) },
    { id: 16, url: 'https://garticphone.com/images/avatar/15.svg', pseudoRandom: 'SkeletonCool' + this.setRandomInt(0,9999) },
    { id: 17, url: 'https://garticphone.com/images/avatar/16.svg', pseudoRandom: 'PiggyCool' + this.setRandomInt(0,9999) },
    { id: 18, url: 'https://garticphone.com/images/avatar/17.svg', pseudoRandom: 'UnicornCool' + this.setRandomInt(0,9999) },
    { id: 19, url: 'https://garticphone.com/images/avatar/18.svg', pseudoRandom: 'VikingCool' + this.setRandomInt(0,9999) },
    { id: 20, url: 'https://garticphone.com/images/avatar/19.svg', pseudoRandom: 'DeathCool' + this.setRandomInt(0,9999) },
    { id: 21, url: 'https://garticphone.com/images/avatar/20.svg', pseudoRandom: 'KnightCool' + this.setRandomInt(0,9999) },
    { id: 22, url: 'https://garticphone.com/images/avatar/21.svg', pseudoRandom: 'BombCool' + this.setRandomInt(0,9999) },
    { id: 23, url: 'https://garticphone.com/images/avatar/22.svg', pseudoRandom: 'PandaCool' + this.setRandomInt(0,9999) },
    { id: 24, url: 'https://garticphone.com/images/avatar/23.svg', pseudoRandom: 'MushroomCool' + this.setRandomInt(0,9999) },
    { id: 25, url: 'https://garticphone.com/images/avatar/24.svg', pseudoRandom: 'PidgetCool' + this.setRandomInt(0,9999) },
    { id: 26, url: 'https://garticphone.com/images/avatar/25.svg', pseudoRandom: 'FlowerCool' + this.setRandomInt(0,9999) },
    { id: 27, url: 'https://garticphone.com/images/avatar/26.svg', pseudoRandom: 'TribalCool' + this.setRandomInt(0,9999) },
    { id: 28, url: 'https://garticphone.com/images/avatar/27.svg', pseudoRandom: 'CalaverasCool' + this.setRandomInt(0,9999) },
    { id: 29, url: 'https://garticphone.com/images/avatar/28.svg', pseudoRandom: 'JalapenosCool' + this.setRandomInt(0,9999) },
    { id: 30, url: 'https://garticphone.com/images/avatar/29.svg', pseudoRandom: 'OwlCool' + this.setRandomInt(0,9999) },
    { id: 31, url: 'https://garticphone.com/images/avatar/30.svg', pseudoRandom: 'CyclopCool' + this.setRandomInt(0,9999) },
    { id: 32, url: 'https://garticphone.com/images/avatar/31.svg', pseudoRandom: 'ReaperCool' + this.setRandomInt(0,9999) },
    { id: 33, url: 'https://garticphone.com/images/avatar/32.svg', pseudoRandom: 'DinosaurCool' + this.setRandomInt(0,9999) },
    { id: 34, url: 'https://garticphone.com/images/avatar/33.svg', pseudoRandom: 'VampireCool' + this.setRandomInt(0,9999) },
    { id: 35, url: 'https://garticphone.com/images/avatar/34.svg', pseudoRandom: 'AstronautCool' + this.setRandomInt(0,9999) },
    { id: 36, url: 'https://garticphone.com/images/avatar/35.svg', pseudoRandom: 'ChickenCool' + this.setRandomInt(0,9999) },
    { id: 37, url: 'https://garticphone.com/images/avatar/36.svg', pseudoRandom: 'GhostCool' + this.setRandomInt(0,9999) },
    { id: 38, url: 'https://garticphone.com/images/avatar/37.svg', pseudoRandom: 'PenguinCool' + this.setRandomInt(0,9999) },
    { id: 39, url: 'https://garticphone.com/images/avatar/38.svg', pseudoRandom: 'PencilCool' + this.setRandomInt(0,9999) },
    { id: 40, url: 'https://garticphone.com/images/avatar/39.svg', pseudoRandom: 'BearCool' + this.setRandomInt(0,9999) },
    { id: 41, url: 'https://garticphone.com/images/avatar/40.svg', pseudoRandom: 'YetiCool' + this.setRandomInt(0,9999) },
    { id: 42, url: 'https://garticphone.com/images/avatar/41.svg', pseudoRandom: 'SnailCool' + this.setRandomInt(0,9999) },
    { id: 43, url: 'https://garticphone.com/images/avatar/42.svg', pseudoRandom: 'LamaCool' + this.setRandomInt(0,9999) },
    { id: 44, url: 'https://garticphone.com/images/avatar/43.svg', pseudoRandom: 'MonkeyCool' + this.setRandomInt(0,9999) },
    { id: 45, url: 'https://garticphone.com/images/avatar/44.svg', pseudoRandom: 'MonsterCool' + this.setRandomInt(0,9999) },
    { id: 46, url: 'https://garticphone.com/images/avatar/45.svg', pseudoRandom: 'ClownCool' + this.setRandomInt(0,9999) },
  ];
  avatarChosen: string | null = '';
  pseudoChosen: string | null = '';
  pseudoPlaceholder: string = '';
  trackedPseudo : string | null = '';
  formCreatePlayer: FormGroup;
  formCreatePlayerSubscription: Subscription | undefined;
  dateToday = new Date();
  // scaleValue: number = 1;


  constructor(
    private renderer: Renderer2,
    private localStorageService: LocalStorageService,
    private playerService: PlayerService,
    private roomService: RoomService) {
    
    this.formCreatePlayer = new FormGroup({
      name: new FormControl(this.localStorageService.getItem('pseudo')),
      avatar: new FormControl('')
    });
  }

  ngOnInit(): void {


    this.setRandomAvatarAndPseudo();
    this.trackPseudoChange();
    // this.updateScale();
    // window.addEventListener('resize', this.updateScale);
  }

  
  ngOnDestroy(): void {
    if (this.formCreatePlayerSubscription) {
      this.formCreatePlayerSubscription.unsubscribe();
    }
    // window.removeEventListener('resize', this.updateScale);
  }

  setRandomAvatarAndPseudo(): void {

    const randomIndex = this.setRandomInt(0, this.avatarsImage.length);

    // Si un avatar n'est pas stocke dans le localStorage alors on genere l'avatar
    if (this.localStorageService.getItem('avatar') == null) {
      // Prend un nombre aleatoire entre 1 et 46 (taille de l'array image avatar)
      this.avatarChosen = this.avatarsImage[randomIndex].url;
    }
    else {
      this.avatarChosen = this.localStorageService.getItem('avatar');
    }

    // Si un pseudo n'est pas stocke dans le localStorage alors on genere le pseudo
    if (this.localStorageService.getItem('pseudo') == null) {
      this.pseudoChosen = this.avatarsImage[randomIndex].pseudoRandom;
    }
    else {
      this.pseudoChosen = this.localStorageService.getItem('pseudo');
      this.trackedPseudo = this.localStorageService.getItem('pseudo');
    }
  }

  onAvatarClick(avatar: AvatarImage): boolean {

    let pseudoInputValue: string = this.formCreatePlayer.get('name')?.value;

    this.avatarChosen = avatar.url;
    this.pseudoPlaceholder = avatar.pseudoRandom;

    if (pseudoInputValue == null || pseudoInputValue == '') {
      // Input empty when click then set avatar selected random pseudo 
      this.pseudoChosen = this.pseudoPlaceholder;

      if (this.trackedPseudo) {
        this.pseudoChosen = this.trackedPseudo;
      }

    } 
    else 
    {
      // Input not empty when click then set last insert input pseudo tracked
      // Comment line below if pseudo need to be randomized each time the user cleared it
      this.pseudoChosen = this.trackedPseudo;
    }
    
      return false;
  }

  trackPseudoChange(): void {
    this.formCreatePlayerSubscription = this.formCreatePlayer.get('name')?.valueChanges.subscribe((value) => {
      
      if (value) {
        this.pseudoChosen = value;
        this.trackedPseudo = value;
      }

    });
  }

  onSubmitFormCreatePlayer() {
    if (this.formCreatePlayer.valid) {
      // Stocke sur la machine user le pseudo et l'avatar choisit
      this.localStorageService.setItem('pseudo', this.pseudoChosen);
      this.localStorageService.setItem('avatar', this.avatarChosen);

      // On set nos valeur final à notre formulaire
      this.formCreatePlayer.get('name')?.setValue(this.pseudoChosen);
      this.formCreatePlayer.get('avatar')?.setValue(this.avatarChosen);

      // Preparation des donnees Room à envoyer au back
      const roomData = new RoomDTO(
        this.setRandomIdAlphaNmericUpperLowerCase(8), // Code secret pour rejoindre la room
        this.dateToday.toLocaleDateString('fr-FR') + ' ' + this.dateToday.toLocaleTimeString('fr-FR') // DateTime creation de la room au format fr
      );

      // Preparation des donnees Player du formulaire à envoyer au back
      const playerData = new PlayerDTO(
        this.formCreatePlayer.get('name')?.value,
        this.formCreatePlayer.get('avatar')?.value
      );
      // Set des donnees additionel au back pour notre Player
      playerData.isHost = true;
      playerData.point = 0;

      // Appel API /addRoom, Creation de la Room en bdd
      this.roomService.addRoom(roomData.toBackendPayload()).subscribe(
        (responseRoom) => {
          console.log('Room created successfully with the id : ' + responseRoom.id, responseRoom);

          playerData.currentRoom = responseRoom;

          this.playerService.addPlayer(playerData.toBackendPayload()).subscribe(
            (responsePlayer) => {
              console.log('Player created successfully:', responsePlayer);
            },
            (error) => {
              console.error('Error creating player:', error);
            }
          );

        },
        (error) => {
          console.error('Error creating room:', error);
        }
      );
      
      // Appel API /addPlayer, Creation du Player en bdd
     

      console.log('Form submitted:', this.formCreatePlayer.value);
    } else {
      console.log('Form is invalid');
    }
  }

  setRandomInt(min: number, max: number): number {
    return Math.floor((Math.random() * max) + min);
  }

  setRandomIdAlphaNmericUpperLowerCase(length: number): string {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    return Array.from({ length: length }, () => characters.charAt(Math.floor(Math.random() * characters.length))).join('');
  }
  // updateScale = (): void => {

  //   if (window.innerWidth > 640) {

  //     var t = window.innerWidth < 1920 ? 180 : 320;
  //     var e = (window.innerWidth - t) / 1150;

  //       if (766 * e > window.innerHeight) {

  //         this.scaleValue = window.innerHeight / 766;

  //       }
  //     }

  // }
}
