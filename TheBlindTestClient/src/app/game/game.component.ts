import {Component, OnInit} from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { PlayerService } from '../services/player/player.service';
import { PlayerDTO } from '../services/player/PlayerDTO';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  public players: PlayerDTO[];

  constructor(private playerService: PlayerService) {
    this.players = [];
  }

  ngOnInit(): void {
    this.getPlayers();

  }

  public getPlayers(): void {
    this.playerService.getPlayers().subscribe(
      (response: PlayerDTO[]) => {
        this.players = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }

    )
  }

  public randomIntFromInterval(min: number, max: number) : number {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

}
