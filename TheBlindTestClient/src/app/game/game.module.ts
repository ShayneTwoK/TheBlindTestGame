import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameRoutingModule } from './game-routing.module';
import { GameComponent } from './game.component';
import {RouterModule} from "@angular/router";
import {PlayerListModule} from "../player-list/player-list.module";
import { PlayerService } from '../services/player/player.service';


@NgModule({ declarations: [
        GameComponent
    ], imports: [CommonModule,
        GameRoutingModule,
        RouterModule,
        PlayerListModule], providers: [PlayerService] })
export class GameModule { }
